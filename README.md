# Symfony 5 Rest API server application

## Endpoints:

* GET /api/v1/pitches

* GET /api/v1/pitches/<id>

* GET /api/v1/pitches/<id>/slots

* POST /api/v1/pitches/<id>/slots

Prefix `/api/v1` added because of the recommended practice of API versioning.
The endpoints do format the response data and accept request body conforming to `JSON:API` specification but I couldn't or didn't know how to make `FOSRestBundle` use `application/vnd.api+json` content type header.

## Architecture

I aimed to implement Multitier architecture, having the following layers: 

* Controllers, data transfer objects and view objects are all in the Presentation layer.

* Service classes are in the Application layer,

* Entity classes make the Business logic layer and

* Repository classes are the Data persistence layer. 

Therefore, FormType classes bind data to a simple data transfer objects in `Form/Model` directory, to avoid direct coupling with Business layer. Requested data is transformed to `View` data transfer objects and serialized for delivery via API endpoints.
This is what makes the application look a bit different than regular Symfony applications.

## Specific Symfony Bundle used

* FOSRestBundle v3

## Tests

I used PHPUnit. `PitchController` is covered by functional tests, although, the only POST method test does not pass yet, needs some more debugging. I added unit tests for Entity classes, and added tests for Service classes (not 100% though) as an example.

## Fixtures

I added fixtures bundle and one class to enable easy generating of necessary records in database for test environment.

## Possible improvements

With more time I would do a better check of how to implement support for `application/vnd.api+json` in FOSRestBundle. As far as I could see it does not have support for JSON content type header other than `application/json`. 

Also it could use more tests coverage, specifically for data transfer objects in `Form/Model` and `View/Model`, FormType classes in `Form/Type` and Repository class. 



