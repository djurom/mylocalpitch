<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;

use App\Service\PitchViewService;
use App\Entity\Pitch;
use App\Form\Type\PitchSlotsType;
use App\Form\Model\PitchSlots;
use App\Service\SlotService;
use App\Service\SlotViewService;

/**
 * Adding common route prefix as per Rest API routing best practices.
 * @Route("/api/v1") 
 */
class PitchController extends AbstractFOSRestController
{
    
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * @Route("/pitches", name="pitches", methods={"GET"})
     */
    public function pitches(PitchViewService $pitchViewService)
    {
        $repository = $this->getDoctrine()->getRepository(Pitch::class);
        $pitches = $repository->findAll();
        $pitchesEnvelope = $pitchViewService->formatPitchViews($pitches);
        
        $view = $this->view($pitchesEnvelope->serialize(), 200);
        return $this->handleView($view);
    }
    
    /**
     * @Route("/pitches/{id}", name="single_pitch", methods={"GET"})
     */
    public function pitch(int $id, PitchViewService $pitchViewService)
    {
        $repository = $this->getDoctrine()->getRepository(Pitch::class);
        $pitch = $repository->find($id);
        $pitchesEnvelope = $pitchViewService->formatPitchViews([$pitch]);

        $view = $this->view($pitchesEnvelope->serialize(), 200);
        return $this->handleView($view);
    }
    
    /**
     * @Route("/pitches/{id}/slots", name="pitch_slots", methods={"GET","POST"})
     */
    public function pitchSlots($id, Request $request, SlotService $slotService, SlotViewService $slotViewService)
    {
        if($request->isMethod('GET')) {
            return $this->getSlots($id, $slotViewService);
            
        } elseif($request->isMethod('POST')) {
            $repository = $this->getDoctrine()->getRepository(Pitch::class);
            $pitch = $repository->find($id);
            if($pitch instanceof Pitch) {
                $slotsData = $request->request->get('data');
                $formData = [];
                foreach($slotsData as $slotData) {
                    $formData['slots'][] = $slotService->formatSlotFormData($slotData);
                }
                return $this->processFormData($formData, $slotService, $pitch);
            }
        }
    }
    
    /**
     * 
     * @param mixed[] $formData
     * @param SlotService $slotService
     * @param Pitch $pitch
     * @return Response
     */
    private function processFormData(array $formData, SlotService $slotService, Pitch $pitch)
    {
        $form = $this->createForm(PitchSlotsType::class, new PitchSlots());
        $form->submit($formData);
        if($form->isSubmitted() && $form->isValid()) {
            $pitchSlots = $form->getData();
            $slotService->transformSlotsData($pitchSlots, $pitch);
            $this->getDoctrine()->getManager()->flush();
            return $this->handleView(
                $this->view(['OK'],
                    Response::HTTP_CREATED
                            )
                );
        } else {
            return $this->handleView($this->view($form, Response::HTTP_BAD_REQUEST));
        }
    }
    
    /**
     * 
     * @param int $pitchId
     * @param SlotViewService $slotViewService
     * @return Response
     */
    private function getSlots(int $pitchId, SlotViewService $slotViewService)
    {
        $repository = $this->getDoctrine()->getRepository(Pitch::class);
        $pitch = $repository->find($pitchId);
        $slots = $pitch->getSlots();
        $slotsView = $slotViewService->formatSlotViews($slots);
        $view = $this->view($slotsView->serialize(), 200);
        return $this->handleView($view);
    }
}