<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Pitch;
use App\Entity\Slot;

use \DateTime;

class PitchFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $slot = new Slot();
        $slot->setStarts(new DateTime("2020-11-04T18:00:00+00:00"))
                ->setEnds(new DateTime("2020-11-04T19:00:00+00:00"))
                ->setPrice("122")
                ->setCurrency("GBP")
                ->setAvailable(true);
                
                
        $pitch = new Pitch();
        $pitch->setName("Walkabout Creek")
                 ->setSport("Badminton")
                 ->addSlot($slot);
        
         
        $slot2 = new Slot();
        $slot2->setStarts(new DateTime("2020-11-04T12:00:00+00:00"))
                ->setEnds(new DateTime("2020-11-04T14:00:00+00:00"))
                ->setPrice("85")
                ->setCurrency("USD")
                ->setAvailable(true);
        
        $slot3 = new Slot();
        $slot3->setStarts(new DateTime("2020-11-04T14:00:00+00:00"))
                ->setEnds(new DateTime("2020-11-04T16:00:00+00:00"))
                ->setPrice("90")
                ->setCurrency("USD")
                ->setAvailable(true);
        
        $pitch2 = new Pitch();
        $pitch2->setName("Tishomingo Plaza")
                 ->setSport("Athletics")
                 ->addSlot($slot2)
                 ->addSlot($slot3);
        
        $manager->persist($pitch);
        $manager->persist($pitch2);

        $manager->flush();
    }
}
