<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=App\Repository\PitchRepository::class)
 */
class Pitch
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $sport;

    /**
     * 
     * @var Slot[]
     * @ORM\OneToMany(targetEntity="App\Entity\Slot", mappedBy="pitch", cascade={"persist"} )
     */
    private $slots;

    public function __construct()
    {
        $this->slots = new ArrayCollection();
    }
    
    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName():string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSport():string
    {
        return $this->sport;
    }
    
    public function getSlots():array
    {
        return $this->slots->toArray();
    }
    

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setSport($sport) {
        $this->sport = $sport;
        return $this;
    }

    
    public function addSlot(Slot $slot)
    {
        $slot->setPitch($this);
        $this->slots->add($slot);
        return $this;
    }
}