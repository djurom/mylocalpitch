<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use \DateTime;

/**
 * @ORM\Entity()
 */
class Slot
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @var datetime
     * @ORM\Column(type="datetime")
     */
    private $starts;
    
    /**
     * @var datetime
     * @ORM\Column(type="datetime")
     */
    private $ends;
    
    /**
     * @var string
     * @ORM\Column(type="integer")
     */
    private $price;
    
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $currency;
    
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $available;
    
    /**
     * @var Pitch
     * @ORM\ManyToOne(targetEntity="App\Entity\Pitch", inversedBy="slots")
     */
    private $pitch;
    
    public function getId() 
    {
        return $this->id;
    }

    public function getStarts(): DateTime
    {
        return $this->starts;
    }

    public function getEnds(): DateTime
    {
        return $this->ends;
    }

    public function getPrice(): string 
    {
        return $this->price;
    }

    public function getCurrency(): string 
    {
        return $this->currency;
    }

    public function getAvailable(): bool 
    {
        return $this->available;
    }

    public function getPitch(): Pitch 
    {
        return $this->pitch;
    }

    public function setId($id) 
    {
        $this->id = $id;
        return $this;
    }

    public function setStarts(DateTime $starts) 
    {
        $this->starts = $starts;
        return $this;
    }

    public function setEnds(DateTime $ends) 
    {
        $this->ends = $ends;
        return $this;
    }

    public function setPrice(string $price) 
    {
        $this->price = $price;
        return $this;
    }

    public function setCurrency(string $currency) 
    {
        $this->currency = $currency;
        return $this;
    }

    public function setAvailable(bool $available) 
    {
        $this->available = $available;
        return $this;
    }

    public function setPitch(Pitch $pitch) 
    {
        $this->pitch = $pitch;
        return $this;
    }

}
