<?php

namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

use App\Form\Model\Slot;

/**
 * Simple data transfer object class, for binding form data.
 */
class PitchSlots
{
    
    /**
     * 
     * @var Slot[]
     * @Assert\Valid
     */
    private $slots;
    
    
    public function __construct()
    {
        $this->slots = [];
    }
    
    /**
     * 
     * @param Slot $slot
     */
    public function addSlot(Slot $slot)
    {
        $this->slots[] = $slot;
    }
   
    /**
     * 
     * @return Slot[]
     */
    public function getSlots(): array 
    {
        return $this->slots;
    }

    /**
     * 
     * @param Slot[] $slots
     */
    public function setSlots(array $slots) 
    {
        $this->slots = $slots;
    }
}
