<?php

namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Simple data transfer object class, for binding form data.
 */
class Slot
{
    /**
     * 
     * @var string
     * @Assert\NotBlank()
     */
    private $starts;
    
    /**
     * 
     * @var string
     * @Assert\NotBlank()
     */
    private $ends;
    
    /**
     * 
     * @var string
     * @Assert\NotBlank()
     * @Assert\Positive
     */
    private $price;
    
    /**
     * 
     * @var string
     * @Assert\NotBlank()
     */
    private $currency;
    
    /**
     * 
     * @var boolean
     */
    private $available;
    
    /**
     * 
     * @return string
     */
    public function getStarts() 
    {
        return $this->starts;
    }

    /**
     * 
     * @return string
     */
    public function getEnds() 
    {
        return $this->ends;
    }

    /**
     * 
     * @return string
     */
    public function getPrice() 
    {
        return $this->price;
    }

    /**
     * 
     * @return string
     */
    public function getCurrency() 
    {
        return $this->currency;
    }

    /**
     * 
     * @return boolean
     */
    public function getAvailable() 
    {
        return $this->available;
    }

    /**
     * 
     * @param string$starts
     * @return $this
     */
    public function setStarts(string $starts) 
    {
        $this->starts = $starts;
        return $this;
    }

    /**
     * 
     * @param string $ends
     * @return $this
     */
    public function setEnds(string $ends) 
    {
        $this->ends = $ends;
        return $this;
    }

    /**
     * 
     * @param string $price
     * @return $this
     */
    public function setPrice(string $price) 
    {
        $this->price = $price;
        return $this;
    }

    /**
     * 
     * @param string $currency
     * @return $this
     */
    public function setCurrency(string $currency) 
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * 
     * @param string $available
     * @return $this
     */
    public function setAvailable(string $available) 
    {
        if($available == "true")
            $this->available = true;
        if($available == "false") 
            $this->available = false;
        
        return $this;
    }

}
