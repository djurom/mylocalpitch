<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Pitch;


class PitchRepository extends ServiceEntityRepository
{
    /**
     * 
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pitch::class);
    }
    
    /**
     * @param mixed $id
     * @param int|null $lockMode
     * @param int|null $lockVersion
     * @return Pitch
     */
    public function find($id, $lockMode = NULL, $lockVersion = NULL)
    {
        $qb = $this->createQueryBuilder('p');
        $qb ->where('p.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $result = $query->execute();
        if(isset($result[0]))
            return $result[0];
    }

    /**
     * @return Pitch[]
     */
    public function findAll():array
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('p');

        $query = $qb->getQuery();
        return $query->execute();
    }
}