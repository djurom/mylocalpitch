<?php 

namespace App\Service;

use App\Entity\Pitch;
use App\View\Model\Pitch as PitchView;
use App\View\Model\Envelope;
use App\View\Model\Meta;

class PitchViewService
{
    /**
     * @param Pitch[] $pitches
     * @return Envelope
     */
    public function formatPitchViews(array $pitches): Envelope
    {
        
        $envelope = new Envelope();
        if(count($pitches) > 1) {
            $meta = new Meta(count($pitches));
            $envelope->setMeta($meta);
        }
        foreach($pitches as $pitch) {
            $pitchView = $this->formatPitchView($pitch);
            $envelope->addDataItem($pitchView);
        }
        
        return $envelope;
    }

    /**
     * 
     * @param Pitch $pitch
     * @return PitchView
     */
    private function formatPitchView(Pitch $pitch): PitchView
    {
        $pitchView = new PitchView($pitch->getId(), $pitch->getName(), $pitch->getSport());
        return $pitchView;
    }
}