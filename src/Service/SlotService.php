<?php

namespace App\Service;

use App\Form\Model\PitchSlots;
use App\Entity\Pitch;
use App\Entity\Slot;

use \DateTime;

class SlotService
{
    /**
     * 
     * @param mixed[] $slotData
     * @return mixed[]
     */
    public function formatSlotFormData(array $slotData):array
    {
        $slotFormData = [];
        $slotFormData['starts'] = $slotData['attributes']['starts'];
        $slotFormData['ends'] = $slotData['attributes']['ends'];
        $slotFormData['price'] = $slotData['attributes']['price'];
        $slotFormData['currency'] = $slotData['attributes']['currency'];
        $slotFormData['available'] = $slotData['attributes']['available'];
        return $slotFormData;
    }
    
    
    /**
     * 
     * @param PitchSlots $pitchSlots
     * @param Pitch $pitch
     */
    public function transformSlotsData(PitchSlots $pitchSlots, Pitch $pitch)
    {
        foreach($pitchSlots->getSlots() as $slotDTO) {
            $slot = new Slot();
            $slot->setStarts(new DateTime($slotDTO->getStarts()))
                    ->setEnds(new DateTime($slotDTO->getEnds()))
                    ->setPrice($slotDTO->getPrice())
                    ->setCurrency($slotDTO->getCurrency())
                    ->setAvailable((bool)$slotDTO->getAvailable());
            $pitch->addSlot($slot);
        }
    }
    
}