<?php

namespace App\Service;

use App\View\Model\Envelope;
use App\Entity\Slot;
use App\View\Model\Slot as SlotView;
use App\View\Model\Meta;


class SlotViewService
{

    /**
     * @param Slot[] $slots
     * @return Envelope
     */
    public function formatSlotViews(array $slots): Envelope
    {
        
        $envelope = new Envelope();
        if(count($slots) > 1) {
            $meta = new Meta(count($slots));
            $envelope->setMeta($meta);
        }
        foreach($slots as $slot) {
            $slotView = $this->formatSlotView($slot);
            $envelope->addDataItem($slotView);
        }
        
        return $envelope;
    }

    /**
     * 
     * @param Slot $slot
     * @return SlotView
     */
    private function formatSlotView(Slot $slot): SlotView
    {
        $slotView = new SlotView();
        $slotView->setId($slot->getId())
                ->setStarts($slot->getStarts())
                ->setEnds($slot->getEnds())
                ->setPrice($slot->getPrice())
                ->setCurrency($slot->getCurrency())
                ->setAvailable($slot->getAvailable());
        
        return $slotView;
    }
    
}