<?php

namespace App\View;


class LabelEnum
{
    const TOTAL_ITEMS = 'total_items';
    
    const META = 'meta';
    
    const DATA = 'data';
    
    const PITCHES = 'pitches';
    
    const SLOTS = 'slots';
}

