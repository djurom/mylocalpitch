<?php 

namespace App\View\Model;

use Doctrine\Common\Collections\ArrayCollection;

use App\View\LabelEnum;
use App\View\Model\Meta;

use \Serializable;

/**
 * Data transfer object class. Represents the top level JSON object.
 */
class Envelope implements Serializable
{
    /**
     * 
     * @var Meta
     */
    private $meta;

    /**
     * @var Serializable[]
     */
    private $data;

    /**
     * @param int $totalItems
     */
    public function __construct()
    {
        $this->data = new ArrayCollection();
    }

    /**
     * 
     * @param Serializable $dataItem
     */
    public function addDataItem(Serializable $dataItem)
    {
        $this->data->add($dataItem);
    }
    
    /**
     * 
     * @param Meta $meta
     */
    public function setMeta(Meta $meta)
    {
        $this->meta = $meta;
    }

    /**
     * 
     * @return mixed[]
     */
    public function serialize()
    {
        $serialized = [];
        if($this->meta instanceof Meta) {
            $serialized[LabelEnum::META] = $this->meta->serialize();
        }
        $serialized[LabelEnum::DATA] = $this->serializeDataItems();
        
        return $serialized;
    }
    
    /**
     * 
     * @return mixed[]
     */
    private function serializeDataItems()
    {
        $items = [];
        if(count($this->data) > 1) {
            foreach($this->data as $dataItem) {
                $items[] = $dataItem->serialize();
            }
            return $items; 
        } elseif(count($this->data)==1) {
            return $this->data[0]->serialize();
        }
    }
    
    public function unserialize($serialized){}
}