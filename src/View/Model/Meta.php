<?php

namespace App\View\Model;

use \Serializable;


class Meta implements Serializable
{
    /**
     * 
     * @var int
     */
    private $totalItems;
    
    /**
     * 
     * @param int $totalItems
     */
    public function __construct(int $totalItems)
    {
        $this->totalItems = $totalItems;
    }
    
    /**
     * 
     * @return mixed[]
     */
    public function serialize()
    {
        return [
            'total_items' => $this->totalItems
        ];
    }
    
    public function unserialize($serialized){}
}