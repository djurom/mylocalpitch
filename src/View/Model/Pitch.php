<?php

namespace App\View\Model;

use App\View\LabelEnum;

use \Serializable;


class Pitch implements Serializable
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var $string
     */
    private $sport;

    /**
     * @param int $id
     * @param string $name 
     * @param string $sport 
     */
    public function __construct(int $id, string $name, string $sport)
    {
        $this->id = $id;
        $this->name = $name;
        $this->sport = $sport;
    }

    public function serialize()
    {
        return [
            'type' => LabelEnum::PITCHES,
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'sport' => $this->sport
            ]
        ];
    }

    public function unserialize($serialized){}

}