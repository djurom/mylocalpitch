<?php

namespace App\View\Model;

use App\View\LabelEnum;

use \Serializable;

class Slot implements Serializable
{
    /**
     * 
     * @var int
     */
    private $id;
    
    /**
     * 
     * @var string
     */
    private $starts;
    
    /**
     * 
     * @var string
     */
    private $ends;
    
    /**
     * 
     * @var string
     */
    private $price;
    
    /**
     * 
     * @var string
     */
    private $currency;
    
    /**
     * 
     * @var string
     */
    private $available;
    
    public function getId(): int 
    {
        return $this->id;
    }

    public function getStarts() 
    {
        return $this->starts;
    }

    public function getEnds() 
    {
        return $this->ends;
    }

    public function getPrice() 
    {
        return $this->price;
    }

    public function getCurrency() 
    {
        return $this->currency;
    }

    public function getAvailable() 
    {
        return $this->available;
    }

    public function setId(int $id) 
    {
        $this->id = $id;
        return $this;
    }

        public function setStarts($starts) 
    {
        $this->starts = $starts;
        return $this;
    }

    public function setEnds($ends) 
    {
        $this->ends = $ends;
        return $this;
    }

    public function setPrice($price) 
    {
        $this->price = $price;
        return $this;
    }

    public function setCurrency($currency) 
    {
        $this->currency = $currency;
        return $this;
    }

    public function setAvailable($available) 
    {
        $this->available = $available;
        return $this;
    }

    public function serialize()
    {
        return [
            'type' => LabelEnum::SLOTS,
            'id' => $this->id,
            'attributes' => [
                'starts' => $this->starts,
                'ends' => $this->ends,
                'price' => $this->price,
                'currency' => $this->currency,
                'available' => $this->available
            ]
        ];
    }
    
    public function unserialize($serialized){}
}
