<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class PitchControllerTest extends WebTestCase
{
    public function testPitches()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/pitches');

        $responseContent = $client->getResponse()->getContent();
        $this->assertEquals(self::pitchesResponse(), $responseContent);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
    
    public function testPitch()
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/pitches/2');

        $responseContent = $client->getResponse()->getContent();
        $this->assertEquals(self::pitchResponse(), $responseContent);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
    
    public function testGetPitchesSlots()
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/pitches/2/slots');

        $responseContent = $client->getResponse()->getContent();
        $this->assertEquals(self::pitchesSlotsResponse(), $responseContent);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
    
//    public function testPostPitchesSlots()
//    {
//        $client = static::createClient([], [
//            'HTTP_CONTENT_TYPE'       => 'application/json',
//        ]);
//        $client->request('POST', '/api/v1/pitches/1/slots', [], [], [], self::pitchesSlotsRequestBody());
//        $responseContent = $client->getResponse()->getContent();
//        $this->assertEquals("OK", $responseContent);
//        $this->assertEquals(201, $client->getResponse()->getStatusCode());
//    }
    
    private static function pitchesResponse()
    {
        $pitches = <<<'EOD'
        {"meta":{"total_items":2},"data":[{"type":"pitches","id":1,"attributes":{"name":"Walkabout Creek","sport":"Badminton"}},{"type":"pitches","id":2,"attributes":{"name":"Tishomingo Plaza","sport":"Athletics"}}]}
        EOD;
        
        return $pitches;
    }
    
    private static function pitchResponse()
    {
        $pitch = <<<'EOD'
        {"data":{"type":"pitches","id":2,"attributes":{"name":"Tishomingo Plaza","sport":"Athletics"}}}
        EOD;
        return $pitch;
    }
    
    private static function pitchesSlotsResponse()
    {
        $pitchesSlots = <<<'EOD'
        {"meta":{"total_items":2},"data":[{"type":"slots","id":2,"attributes":{"starts":"2020-11-04T12:00:00+00:00","ends":"2020-11-04T14:00:00+00:00","price":"85","currency":"USD","available":true}},{"type":"slots","id":3,"attributes":{"starts":"2020-11-04T14:00:00+00:00","ends":"2020-11-04T16:00:00+00:00","price":"90","currency":"USD","available":true}}]}
        EOD;
        
        return $pitchesSlots;
    }
    
    private static function pitchesSlotsRequestBody()
    {
        $pitchesSlots = <<<'EOD'
        {"data":[{"type":"slots","id":"null","attributes":{"starts":"2021-11-04T18:00:00+00:00","ends":"2021-11-04T19:00:00+00:00","price":"117.00","currency":"GBP","available":"true"}},{"type":"slots","id":"null","attributes":{"starts":"2021-11-04T19:00:00+00:00","ends":"2021-11-04T21:00:00+00:00","price":"119.00","currency":"GBP","available":"false"}}]}
        EOD;
        return $pitchesSlots;
    }
}

