<?php

namespace App\Tests\Entity;
use Doctrine\Common\Collections\ArrayCollection;

use App\Entity\Pitch;
use App\Entity\Slot;

use PHPUnit\Framework\TestCase;


class PitchTest extends TestCase
{
    
    public function testConstructor()
    {
        $pitch = new Pitch();
        $this->assertTrue($pitch instanceof Pitch);
        $this->assertTrue(is_array($pitch->getSlots()));
    }
    
    public function testSetGetName()
    {
        $pitch = new Pitch();
        $name = "Some Name";
        $pitch->setName($name);
        $this->assertEquals($name, $pitch->getName());
    }
    
    public function testSetGetSport()
    {
        $pitch = new Pitch();
        $sport = "Football";
        $pitch->setSport($sport);
        $this->assertEquals($sport, $pitch->getSport());
    }
    
    public function testAddSlot()
    {
        $pitch = new Pitch();
        $slot = new Slot();
        $pitch->addSlot($slot);
        $this->assertTrue($slot === $pitch->getSlots()[0]);
    }
}
