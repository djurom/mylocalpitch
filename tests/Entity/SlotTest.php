<?php

namespace App\Tests\Entity;

use App\Entity\Slot;
use App\Entity\Pitch;

use PHPUnit\Framework\TestCase;

use \DateTime;


class SlotTest extends TestCase
{
    public function testSetGetStarts()
    {
        $slot = new Slot();
        $starts = new DateTime("2021-03-11 17:00:00");
        $slot->setStarts($starts);
        $this->assertTrue($starts === $slot->getStarts());
    }
    
    public function testSetGetEnds()
    {
        $slot = new Slot();
        $ends = new DateTime("2021-03-11 18:00:00");
        $slot->setEnds($ends);
        $this->assertTrue($ends === $slot->getEnds());
    }
    
    public function testSetGetPrice()
    {
        $slot = new Slot();
        $price = "20.05";
        $slot->setPrice($price);
        $this->assertEquals($price, $slot->getPrice());
    }
    
    public function testSetGetCurrency()
    {
        $slot = new Slot();
        $currency = "GBP";
        $slot->setCurrency($currency);
        $this->assertEquals($currency, $slot->getCurrency());
    }
    
    public function testSetGetAvailable()
    {
        $available = true;
        $slot = new Slot();
        $slot->setAvailable(true);
        $this->assertEquals($available, $slot->getAvailable());
    }
    
    public function testSetGetPitch()
    {
        $slot = new Slot();
        $pitch = new Pitch();
        $slot->setPitch($pitch);
        $this->assertTrue($pitch === $slot->getPitch());
    }
}