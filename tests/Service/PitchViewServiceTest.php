<?php

namespace App\Tests\Service;

use App\View\Model\Envelope;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PitchViewServiceTest extends WebTestCase
{
   
    public function testFormatPitchViews()
    {
        self::bootKernel();
        $container = self::$container;
        // Creating Mock objects with 'getId' as an overridable stub.
        $pitch1 = $this->getMockBuilder('App\Entity\Pitch')->setMethods(array('getId'))->getMock();
        $pitch2 = $this->getMockBuilder('App\Entity\Pitch')->setMethods(array('getId'))->getMock();
        $pitch1->setName("Pitch 1")
                ->setSport("Soccer");
        $pitch2->setName("Pitch 2")
                ->setSport("Basketball");
        $pitch1->method('getId')->willReturn(1);
        $pitch2->method('getId')->willReturn(2);
        
        
        $envelope = $container->get("App\Service\PitchViewService")->formatPitchViews([$pitch1, $pitch2]);
        $this->assertTrue($envelope instanceof Envelope);
    }
    
    
    
}
