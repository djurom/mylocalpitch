<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SlotServiceTest extends WebTestCase
{
    public function testFormatSlotFormData()
    {
        self::bootKernel();
        $container = self::$container;
        $slotFormData = $this->provideSlotFormData();
        $slotData = $container->get("App\Service\SlotService")->formatSlotFormData($slotFormData);
        $expectedData = $this->provideExpectedSlotData();
        $this->assertEquals($expectedData, $slotData);
    }
    
    /**
     * 
     * @return string[]
     */
    private function provideSlotFormData()
    {
        return [
            'attributes' => [
                'starts' => '2020-09-09 12:00:00',
                'ends' => '2020-09-09 13:00:00',
                'price' => '45',
                'currency' => 'EUR',
                'available' => 'false'
            ]
        ];
    }
    
    private function provideExpectedSlotData()
    {
        return [
                "starts"=>"2020-09-09 12:00:00",
                "ends"=>"2020-09-09 13:00:00",
                "price"=>"45",
                "currency"=>"EUR",
                "available"=>"false"
            ];

    }
}
