<?php

namespace App\Tests\Service;

use App\View\Model\Envelope;
use App\Entity\Slot;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use \DateTime;

class SlotViewServiceTest extends WebTestCase
{
  public function testFormatSlotViews()
  {
      self::bootKernel();
      $container = self::$container;
      
      // Creating Mock object with 'getId' as an overridable stub.
      $slot = $this->getMockBuilder('App\Entity\Slot')->setMethods(array('getId'))->getMock();
      $slot->method('getId')->willReturn(1);
      $this->populateSlot($slot);
      $envelope = $container->get("App\Service\SlotViewService")->formatSlotViews([$slot]);
      $this->assertTrue($envelope instanceof Envelope);
  }
  
  
  private function populateSlot(Slot $slot)
  {
      $slot->setStarts(new DateTime('2020-09-09 12:00:00'))
              ->setEnds(new DateTime('2020-09-09 13:00:00'))
              ->setPrice('55')
              ->setCurrency('GBP')
              ->setAvailable(true);
  }
}
